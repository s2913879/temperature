package nl.utwente.di.temperatureConverter;

public class Converter {
    public double toFahrenheit(double celsiusTemp) {
        return celsiusTemp * 1.8 + 32;
    }
}
